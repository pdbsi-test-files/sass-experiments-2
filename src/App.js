
import './scss/test.scss';

function App() {
  return (
    <>
    {/* nav bar start */}
    <div id="navbar">
        <div className="logo">
          <h3>Logo here</h3>
        </div>
        <div>
          <ul className="nav-list">
            <li><a href="#main">main</a></li>
            <li><a href="#box">box</a></li>
            <li><a href="#footer">footer</a></li>
          </ul>
        </div>
        <div className="nav-end">
          <h3>nav4</h3>
        </div>
    </div>
    {/* navbar end */}
    {/* main start */}
    <div id="main">
      <h1 className="header">Hello World</h1>
      <p className="paragraph">hello this is styling from sass</p>
      <div className="container">
        <div className="box1">BOX 1</div>
        <div className="box2">BOX 2</div>
        <div className="box3">BOX 3</div>
      </div>
      <div id="box">BOX 4</div>
      <div className="button-container">
        <button className="button">I am a button</button>
      </div>
    </div>
    {/* main end */}
    {/* footer start */}
    <div id="footer">
      <div className="footer-text">This is the footer</div>
    </div>
    {/* footer end */}
    </>
  );
}

export default App;
